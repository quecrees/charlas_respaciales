
<!-- README.md is generated from README.Rmd. Please edit that file -->

## Perfiles topográficos a partir de puntos en R <img src="img/rs_logo2.png" align="right" width="110"/>

[Ver la charla en Youtube](https://youtu.be/fEqANQBuU9s?)

> Paquetes necesarios para el proceso.

``` r
suppressPackageStartupMessages({
  library('tidyverse')
  library('raster')
  library('sf')
  library('tmap')
})
```

> Datos de ejemplo para el análisis.

``` r
station_points <- read_csv("data/stations_points.csv")
>  Parsed with column specification:
>  cols(
>    Sitio = col_character(),
>    X = col_double(),
>    Y = col_double(),
>    Altitud = col_double()
>  )
altitud_dem <- raster("data/dem.tif")
```

### Procedimiento

> Transfromar los datos a puntos espaciales.

> Transformar los datos a líneas.

> Extraer los valores del raster con sus coordenadas.

> Graficar el perfil topográfico.

![](img/day_obj.png)

### Resultado

![](img/profile_stations1.png)

### A manera de conclusión…

> A pesar de que el proceso puede ser bastante intuitivo, existen varios
> detalles que hay que tener en cuenta para obtener resultados
> coherentes. (e.g. `do_union = F`, `along = T`, extraer la distancia
> correcta, etc). Para mayor información acerca de `do_union` revisar
> <https://github.com/r-spatial/sf/issues/692> Además, revisar el libro
> [Geocomputation with R -
> Chapter 5](https://geocompr.robinlovelace.net/)
